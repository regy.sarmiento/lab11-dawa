/*
const mongoose = require("mongoose");

const uniqueValidator = require("mongoose-unique-validator")

let Schema = mongoose.Schema;

let rolesValidos = {
  values: [
    'ADMIN_ROLE',
    'USER_ROLE'
  ],
  message: '{VALUE} no es un rol valido'
}

let usuarioSchema = new Schema({
  nombre: {
    type: String,
    required: [true, "El nombre es necesario"],
  },
  email: {
    type: String,
    unique: true,
    required: [true, "El correo es necesario"],
  },

  password: {
    type: String,
    required: [true, "El password es necesario"],
  },

  role: {
    type: String,
    default: 'USER_ROLE',
    enum: rolesValidos
  },

  estado: {
    type: Boolean,
    default: true,
  }, //Boolean  

});

module.exports = mongoose.model('Usuario', usuarioSchema)
*/

const mongoose = require("mongoose");

let Schema = mongoose.Schema;

let rolesValidos = {
    values: [
        'ADMIN_ROLE',
        'USER_ROLE'
    ],
    message: '{VALUE} no es un rol valido'
}

let usuarioSchema = new Schema({
    nombre: {
        type: String,
        required: [true, "El nombre es necesario"],
    },
    email: {
        type: String,
        unique: true,
        required: [true, "El correo es necesario"],
    },
    password: {
        type: String,
        required: [true, "El password es necesario"],
    },
    role: {
        type: String,
        default: 'USER_ROLE',
        enum: rolesValidos
    },
    estado: {
        type: Boolean,
        default: true,
    },

    img: {
      type: String,
      required: false,
    },

    profilePicture: {
      type: String,
      required: false,
    },

    google: {
      type: Boolean,
      default: false
    }
});

module.exports = mongoose.model('Usuario', usuarioSchema)








/*
 img: {
    type: String,
    required: false,
  }, //la imagen no es obligatoria
  



google: {
  type: Boolean,
  default: false,
}, //Boolean
*/


/*
usuarioSchema.methods.toJSON = function(){
  let user = this
  let userObject = user.toObject()
  delete userObject.password

  return userObject
}


usuarioSchema.plugin(uniqueValidator, {
  message: '{PATH} debe de ser unico'
})
*/
